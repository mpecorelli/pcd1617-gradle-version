package pcd.ass03.ex3;

import pcd.ass03.ex3.acme.TemperatureSensorA1;

/**
 * An example of usage of sensor A1
 * 
 * @author aricci
 *
 */
public class TestSensor {

	public static void main(String[] args) {
		
		TemperatureSensorA1 sensor = new TemperatureSensorA1();

		/*
		 * Reading the sensor every 250 ms for 5 seconds
		 */
		long t0 = System.currentTimeMillis();
		while (System.currentTimeMillis() - t0 < 10000){
			System.out.println(sensor.getCurrentValue());
			try {
				Thread.sleep(250);
			} catch (Exception ex){}
		}
	}
		
}

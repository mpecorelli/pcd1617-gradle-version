package pcd.ass01.ex1.sol;

public class MandelbrotWorker extends Thread {

	private int delta;
	private int nIterMax, xStart;
	private MandelbrotSetImage image; 
	
	public MandelbrotWorker(MandelbrotSetImage image, int nIterMax, int xStart, int delta){
		this.image = image;
		this.nIterMax = nIterMax;
		this.delta = delta;
		this.xStart = xStart;
	}
	
	public void run(){
		int y = 0;
		int x = xStart;
		int oldx = 0;
		log("Computing points from ("+x+",0) every "+delta);
		while (y < image.getHeight()){
			image.compute(x,y,nIterMax);
			oldx = x;
			x = (x + delta) % image.getWidth();
			if (x < oldx){
				y++;
			}
		}
		log("Done.");
	}
	
	protected void log(String msg){
		System.out.println("["+getName()+"] "+msg);
	}
}

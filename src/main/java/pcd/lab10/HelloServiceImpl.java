package pcd.lab10;

import java.rmi.RemoteException;

public class HelloServiceImpl implements HelloService {
        
    public HelloServiceImpl() {}

    public synchronized String sayHello() {
    	return "Hello, world!";
    }
    
    public synchronized String sayHello(int n) {
        return "Hello, world! ==> " + n;
    }

    public synchronized void sayHello(Message m) {
        System.out.println("hello: "+m.getContent());
     
    }

    public /* synchronized */ String sayHello(MyClass obj) throws RemoteException {
    	obj.update(obj.get()+1);
        return "Hello, world! ==> " + obj.get();
    }
        
}